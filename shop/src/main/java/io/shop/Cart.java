package io.shop;

import java.util.ArrayList;
import java.util.List;

public class Cart {
    private List<Product> products = new ArrayList<>();


    public void add(Product product) {
        products.add(product);
    }

    public void clear() {
        products.clear();
    }

    public List<Product> getProducts() {
        return products;
    }

    public double getSum() {
        return products.stream().mapToDouble(Product::getPrice).sum();
    }
}
