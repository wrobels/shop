package io.shop;

import java.util.LinkedList;
import java.util.List;

public class EventEmitter {
    private List<EventListener> observers;

    public EventEmitter() {
        observers = new LinkedList<>();
    }

    public void setOnEventListener(EventListener observer) {
        observers.add(observer);
    }

    public void eventReceived(Event event) {
        for (EventListener observer : observers) {
            observer.onEvent(event);
        }
    }
}
