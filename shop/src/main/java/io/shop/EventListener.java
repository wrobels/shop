package io.shop;

public interface EventListener {
    void onEvent(Event e);
}
