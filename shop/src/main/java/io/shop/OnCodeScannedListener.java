package io.shop;

public interface OnCodeScannedListener {
    void onScanned(int code);
}
