package io.shop;

import io.shop.db.DatabaseMock;
import io.shop.io.LcdDisplay;
import io.shop.io.Printer;
import java.util.Optional;

public class App {
    private CodeScanner codeScanner;
    private EventEmitter eventEmitter;
    private LcdDisplay lcdDisplay;
    private Printer printer;

    Cart cart = new Cart();

    public App(CodeScanner codeScanner, EventEmitter eventEmitter, LcdDisplay lcdDisplay, Printer printer) {
        this.codeScanner = codeScanner;
        this.eventEmitter = eventEmitter;
        this.lcdDisplay = lcdDisplay;
        this.printer = printer;
        this.registerListeners();
    }

    private void registerListeners() {
        codeScanner.setOnCodeScannedListener(code -> {
            Optional<Product> product = DatabaseMock.INSTANCE.getProduct(code);
            if (product.isPresent()) {
                cart.add(product.get());
                lcdDisplay.show("Product scanned: " + product.get().getName() + " Price: " +product.get().getPrice() + "\nTotal sum: " + String.valueOf(cart.getSum()));
            }
            else
                lcdDisplay.show("Product not found");
        });
        eventEmitter.setOnEventListener(event -> {
            if (event == Event.EXIT) {
                lcdDisplay.show(Event.EXIT.getMessage());
            } else if (event == Event.CHECKOUT) {
                printer.show("Products summary: " + String.valueOf(cart.getSum()));
                cart.getProducts().forEach(product -> printer.show(product.getName()));
                lcdDisplay.show("Total sum: " + String.valueOf(cart.getSum()));
                cart.clear();
                lcdDisplay.show(Event.CHECKOUT.getMessage());
            }
        });
    }

}
