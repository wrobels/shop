package io.shop.io;

public interface Display {
    void show(String text);
}
