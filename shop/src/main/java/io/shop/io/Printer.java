package io.shop.io;

public class Printer implements Display {
    @Override
    public void show(String text) {
        System.out.println(text);
    }
}
