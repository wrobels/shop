package io.shop;


public enum Event {
    CHECKOUT("checkout"),
    EXIT("app exit");

    private final String eventType;

    Event(String eventType) {
        this.eventType = eventType;
    }

    public String getMessage() {
        return eventType;
    }
}
