package io.shop;

import java.util.LinkedList;
import java.util.List;

public class CodeScanner {
    private List<OnCodeScannedListener> observers;

    public CodeScanner() {
        observers = new LinkedList<>();
    }

    public void setOnCodeScannedListener(OnCodeScannedListener observer) {
        observers.add(observer);
    }

    public void codeReceived(int code) {
        for (OnCodeScannedListener observer : observers) {
            observer.onScanned(code);
        }
    }
}
