package io.shop.db;

import io.shop.Product;

import java.util.*;

public enum DatabaseMock {

    INSTANCE;

    private Map<Integer, Product> map = new HashMap<>();

    DatabaseMock() {
        List<Product> productList = Arrays.asList(
                new Product(1, "kamien", 0.9),
                new Product(2, "papier", 3.1),
                new Product(3, "nozyce", 9.5)
                // TODO: maybe add some more products?
        );
        productList.forEach(product -> map.put(product.getId(), product));
    }

    public Collection<Product> getProducts() {
        return map.values();
    }

    public Optional<Product> getProduct(int code) {
        if (map.containsKey(code))
            return Optional.of(map.get(code));
        else return
                Optional.empty();
    }

}
