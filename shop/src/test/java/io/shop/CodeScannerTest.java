package io.shop;


import io.shop.io.Display;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class CodeScannerTest {

    private CodeScanner codeScanner;
    @Mock
    private Display lcdDisplay;

    @Before
    public void beforeEach() {
        codeScanner = new CodeScanner();
        codeScanner.setOnCodeScannedListener(this::onScanned);
    }

    @Test
    public void test() {
        codeScanner.codeReceived(321);
        codeScanner.codeReceived(123);
        verify(lcdDisplay, times(1)).show("123");
        verify(lcdDisplay, times(1)).show("321");
    }

    private void onScanned(int code) {
        lcdDisplay.show(String.valueOf(code));
    }


}
