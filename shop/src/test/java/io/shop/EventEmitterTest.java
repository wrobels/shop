package io.shop;

import io.shop.io.Display;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class EventEmitterTest {

    private EventEmitter eventEmitter;
    @Mock
    private Display lcdDisplay;

    @Before
    public void beforeEach() {
        eventEmitter = new EventEmitter();
        eventEmitter.setOnEventListener(this::onEventReceived);
    }

    private void onEventReceived(Event event) {
        switch (event) {
            case EXIT:
                lcdDisplay.show(Event.EXIT.getMessage());
                break;
            case CHECKOUT:
                lcdDisplay.show(Event.CHECKOUT.getMessage());
                break;
        }
    }

    @Test
    public void test() {
        eventEmitter.eventReceived(Event.CHECKOUT);
        eventEmitter.eventReceived(Event.EXIT);
        verify(lcdDisplay, times(1)).show("app exit");
        verify(lcdDisplay, times(1)).show("checkout");
    }

}

