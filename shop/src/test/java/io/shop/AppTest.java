package io.shop;

import io.shop.io.LcdDisplay;
import io.shop.io.Printer;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class AppTest {

    CodeScanner codeScanner;
    EventEmitter eventEmitter;
    App app;

    @Mock
    private LcdDisplay lcdDisplay;
    @Mock
    private Printer printer;

    @Before
    public void beforeEach() {
        codeScanner = new CodeScanner();
        eventEmitter = new EventEmitter();
        app = new App(codeScanner, eventEmitter, lcdDisplay, printer);
    }

    @Test
    public void testScanExistingProduct() {

        codeScanner.codeReceived(1);
        String txt = "Product scanned: kamien Price: 0.9\nTotal sum: " + "0.9";
        verify(lcdDisplay).show(txt);
    }

    @Test
    public void testCheckout() {
        eventEmitter.eventReceived(Event.CHECKOUT);
        verify(printer).show("Products summary: 0.0");
        verify(lcdDisplay).show("Total sum: 0.0");
    }

    @Test
    public void testWrongCode() {
        codeScanner.codeReceived(4);
        verify(lcdDisplay).show("Product not found");
    }

    @Test
    public void testExit(){
        eventEmitter.eventReceived(Event.EXIT);
        verify(lcdDisplay).show("app exit");

    }
}

