package io.shop;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CartTest {

    @Test
    public void test_sumCostOfAllItemsInTheCart() {
        // given
        Cart cart = new Cart();
        cart.add(new Product(1, "gruha", 3));
        cart.add(new Product(2, "japko", 2));
        cart.add(new Product(3, "ziemniok", 0.5));
        // when
        double sum = cart.getSum();
        // then
        assertEquals(sum, 5.5, 0);
    }

}
