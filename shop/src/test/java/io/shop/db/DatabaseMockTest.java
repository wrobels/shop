package io.shop.db;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DatabaseMockTest {

    @Test
    public void testRetrieveAllItemsFromDb() {
        assertEquals(3, DatabaseMock.INSTANCE.getProducts().size());
    }

    @Test
    public void testRetrieveOneItemFromDb() {
        assertEquals("papier", DatabaseMock.INSTANCE.getProduct(2).get().getName());
    }

}
